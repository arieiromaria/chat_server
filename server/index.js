var server = require('ws').Server;
var s = new server({port : 5001});

var name;

s.on('connection', function(ws) {
    ws.on('message',function(message) {

        message = JSON.parse(message);

        //client name
        if(message.type == "name"){
            ws.personName = message.data;
            return;
        }

        console.log("Received: " + message);

        //broadcast to all connect clients
        s.clients.forEach(function e(client){
            if(client != ws)
                client.send(JSON.stringify({
                    name: ws.personName,
                    data: message.data
                }));
        });
    });

    //desconnection from the client side
    ws.on('close', function(){
        console.log("I lost a client!")
    });

    console.log("one more client connected");
});